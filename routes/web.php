<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
//Route::get('/', function () {
//    return view('welcome');
//});

use App\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

// Display All Tasks
Route::get('/', function () {
    $task=Task::orderBy('created_at', 'asc')->get();

    $task=Task::find('1');
    dd($task);

    return view('task');
});

// Add A New Task
Route::post('/task', function (Request $request) {

//    dd($request);

    $validator = Validator::make($request->all(), [
        'name' => 'required|max:255',
    ]);

    if ($validator->fails()) {
        return redirect('/')
            ->withInput()
            ->withErrors($validator);
    }

    // Create The Task...
    $task = new Task;
    $task->name = $request->name;
    $task->save();

    return redirect('/');
});


//Delete An Existing Task

Route::delete('/task/{id}', function ($id) {
    //
});

//// Authentication Routes...
//Route::get('auth/login', 'Auth\AuthController@getLogin');
//Route::post('auth/login', 'Auth\AuthController@postLogin');
//Route::get('auth/logout', 'Auth\AuthController@getLogout');
//
//// Registration Routes...
//Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');
